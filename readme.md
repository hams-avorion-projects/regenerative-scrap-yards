# Regenerative Scrap Yards

This mod makes Scrapyards regenerate wreckages when there are not many of them left in the sector. It only happens from time to time, maybe you have to wait until it happens. Duration can be configured in galaxy folder `moddata/ConfigLib/` as usely in my mods.